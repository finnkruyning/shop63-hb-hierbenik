$(document).ready(function(){
    init_favorites_dynamic();

    $('body').on('click', '.open_favorites_panel, .close_dynamic_favorites', function(e) {
        e.preventDefault();
        $(this).toggleClass('open');
        $('.favorites_dynamic_panel').toggleClass('open');
        $('body, html').toggleClass('no-y-scroll');
    });

    $('body').on('click', '.thumbnail-card-label .delete_fav_design, .thumbnail-card-label .add_fav_design', function(e) {
        load_favorites_dynamic();
    });

    $('body').on('click', '.dynamic_favorites_del', function(e) {
        if(confirm('Weet je het zeker?')) {
            e.preventDefault();
            favClick(this);
            $(this).parent().remove();
            update_favorites_dynamic_count();
        } else {
            return false;
        }
    });
});

function init_favorites_dynamic() {
    var side_panel = $('<div>');
    side_panel
        .addClass('favorites_dynamic_panel')
        .append('<span class="close_dynamic_favorites">&times;</span>')
        .append('<div class="favorites_dynamic_title">Favorieten</div>')
        .append('<p>Hier worden jouw favoriete kaartjes bewaard. Klik op een kaart om de details te bekijken.</p>')
        .append('<div class="dynamic_favorites_item_wrapper" />');

    $('body').prepend(side_panel);
    load_favorites_dynamic();
};

function load_favorites_dynamic() {
    var dynamic_item_wrapper = $('.dynamic_favorites_item_wrapper');
    dynamic_item_wrapper.html('');

    var favorites_request = $.ajax({
        url: "/favorites_api",
        type: "post"
    });

    favorites_request.done(function (response, textStatus, jqXHR) {
        if(response != 'not_logged_in') {
            $.each($.parseJSON(response), function(i, val) {
                var this_item = $('<div>');
                this_item
                    .addClass('favorites_dynamic_item')
                    .append('<a href="/choose_user_card/'+ val.id + '"><img src="/user_design_thumb/' + val.id + '" /></a>')
                    .append('<a href="" data-id="' + val.design_id + '" class="delete_fav_design favorite-button dynamic_favorites_del"><i class="icon icon-unfavorite-heart icon-x2"></i></a>');
                dynamic_item_wrapper.append(this_item);
            });

            update_favorites_dynamic_count();
        } else {

        }
    });
}

function update_favorites_dynamic_count() {
    var fdc = $('.dynamic_favorites_item_wrapper .favorites_dynamic_item').length;
    $('.dynamic_favorites_count').html(fdc);
}
