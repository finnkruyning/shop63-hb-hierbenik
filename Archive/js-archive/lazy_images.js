$(function() {

    // This is "probably" IE9 compatible but will need some fallbacks for IE8
    // - (event listeners, forEach loop)

    // wait for the entire page to finish loading

        // setTimeout to simulate the delay from a real page load
        setTimeout(lazyLoad, 1);

    


    var wrap = function(element, wrapper) {
        element.parentElement.insertBefore(wrapper, element);
        wrapper.appendChild(element);
    };

    var addClassName = function(element, className) {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    };

    function lazyLoad() {

        console.log('go');

        var lazyImages = document.querySelectorAll('.js-lazy-load');

        // loop over each lazy load image
        Array.prototype.forEach.call(lazyImages, function(lazyImage, i) {

            if ($(lazyImage).hasClass('lazy-load')) { // Images
                var imageUrl = lazyImage.getAttribute('src');
                var imageWrapper = document.createElement("div");
                var lazyLoadlarge = document.createElement("img");

                imageWrapper.className = 'lazy-load-wrapper';
                imageWrapper.style.backgroundImage = 'url(' + imageUrl + ')';
                lazyLoadlarge.className = 'lazy-load-large';

                wrap(lazyImage, imageWrapper);

                imageWrapper.appendChild(lazyLoadlarge);

                // listen for load event when the new photo is finished loading
                lazyLoadlarge.addEventListener('load', function() {
                    // add a class to remove the blur filter to smoothly transition the image change
                    setTimeout(function() {
                        imageWrapper.className = imageWrapper.className + ' is-loaded';

                    }, 100);
                });

                lazyLoadlarge.src = imageUrl.replace('lowres', '1x');

            } else if ($(lazyImage).hasClass('lazy-load-background')) { // Background images
                var imageUrl = $(lazyImage).css('background-image');
                imageUrl = imageUrl.replace(/(url\(|\)|")/g, '');

                var imageContainer = document.createElement("div");
                var lazyLoadlarge = document.createElement("img");

                imageContainer.className = 'lazy-load-container';
                imageContainer.style.backgroundImage = lazyImage.style.backgroundImage;
                imageContainer.style.backgroundSize = $(lazyImage).css('background-size');
                imageContainer.style.backgroundPosition = $(lazyImage).css('background-position');

                var largeImageContainer = imageContainer.cloneNode(true);
                largeImageContainer.className = 'lazy-load-container-large';

                $(lazyImage).css('background-image', 'none')

                lazyImage.appendChild(imageContainer);

                lazyLoadlarge.addEventListener('load', function() {
                    console.log('loaded');
                    // add a class to remove the blur filter to smoothly transition the image change
                    setTimeout(function() {
                        largeImageContainer.style.backgroundImage = 'url(' + imageUrl.replace('lowres', '1x') + ')';
                        lazyImage.appendChild(largeImageContainer);

                        lazyImage.className = lazyImage.className + ' is-loaded';
                    }, 100);
                });

                lazyLoadlarge.src = imageUrl.replace('lowres', '1x');

            }

        });
    }
});