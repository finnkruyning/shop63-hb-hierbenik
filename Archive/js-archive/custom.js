$(document).ready(function(){
    if($('.kleur')) {
        $('.kleur .tag-li').each(function () {
            var $hoverText= $(this).find('label').clone().children().remove().end().text().trim();
            $(this).append('<span class="tooltip"></span>');
            $(this).find('.tooltip').html($hoverText);
        })
    }
});
