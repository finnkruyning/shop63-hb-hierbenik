
/*
    SHOW MORE COMPONENT

    Needs cp-show-more.css and show-more.js

    Use class cp-show-more on ul of items and id to link navigation
    Use data-show-more-start for ititial items and data-show-more-more for increment.

    <ul class="cp-show-more" data-show-more-start="2" data-show-more-more="2" id="show-more-videos">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>

    <div class="row cp-show-more-navigation" data-show-more-id="show-more-videos">
        <div class="sm-6"><button class="cp-show-more-more">Toon meer</button></div>
        <div class="sm-6"><button class="cp-show-more-less">Toon minder</button></div>
    </div>

*/


$(document).ready(function () {

    var showMore = $('ul.cp-show-more');

    showMore.each(function () {


        var that = this;

        var startAmount = $(that).data('show-more-start') ? $(that).data('show-more-start') : 4;
        var shown = startAmount;
        var moreAmount = $(that).data('show-more-more') ? $(that).data('show-more-more') : 4;
        var itemsAmount = $(that).children('li').length;

        $(that).children('li:lt('+startAmount+')').fadeIn();

        if (itemsAmount > startAmount){
            $('body').find('div[data-show-more-id='+$(that).attr('id')+'] .cp-show-more-more').fadeIn();
        }

        $('body').on('click', 'div[data-show-more-id='+$(that).attr('id')+'] .cp-show-more-more',function () {
            shown = $(that).children('li:visible').size()+moreAmount;
            if(shown < itemsAmount) {
                $(that).children('li:lt('+shown+')').fadeIn();
            } else {
                $(that).children('li:lt('+itemsAmount+')').fadeIn();
                $('body').find('div[data-show-more-id='+$(that).attr('id')+'] .cp-show-more-more').fadeOut(function(){
                    $('body').find('div[data-show-more-id='+$(that).attr('id')+'] .cp-show-more-less').fadeIn();
                });
           }
        });
        $('body').on('click', 'div[data-show-more-id='+$(that).attr('id')+'] .cp-show-more-less', function () {
            $(that).children('li').not(':lt('+startAmount+')').fadeOut();

            $('body').find('div[data-show-more-id='+$(that).attr('id')+'] .cp-show-more-less').fadeOut(function(){
                if (itemsAmount > startAmount){
                    $('body').find('div[data-show-more-id='+$(that).attr('id')+'] .cp-show-more-more').fadeIn();
                }
            });


        });
    });
});