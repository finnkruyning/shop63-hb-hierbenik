$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header

    if($('#body_design')) {
        var backLink = $('.crumb-mobile');

        $('#crumbs').append(backLink);
    }

    if($('#sidebar')) {
        $("nav #filter_cards").prepend('<h3>Filters</h3><a class="filter-w" href="javascript:;">' +
            '<i class="fa fa-refresh"></i>Filters wissen</a>');
    }


    if($('.kleur')) {
        $('.kleur .tag-li').each(function () {
            var $hoverText= $(this).find('label').clone().children().remove().end().text().trim();
            $(this).append('<span class="tooltip"></span>');
            $(this).find('.tooltip').html($hoverText);
        })
    }





});
