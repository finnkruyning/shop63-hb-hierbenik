$(function () {
   $('.js-foldmenu-header').on('click', function(){

       var foldmenu = $(this).parent('.js-foldmenu');
       var icon = $(this).find('.icon');
       var target = foldmenu.find('.js-foldmenu-body');

       target.toggleClass('show');

       if (target.hasClass('show')){
           icon.removeClass('icon-arrow-bold-down').addClass('icon-arrow-bold-up');
       }
       else {
           icon.removeClass('icon-arrow-bold-up').addClass('icon-arrow-bold-down');
       }
   })

});
