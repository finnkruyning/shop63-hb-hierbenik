(function ($) {
    $.fn.modal = function (options) {
        var defaultOptions = {
            width: '500px',
            target: '.modal',
            isOpen: false,
            openDelay: 0
        };

        options = $.extend({}, defaultOptions, options);

        function _openModal() {
            setTimeout(function () {
                if (!$('html, body').hasClass('lock-modal')) {
                    $('html, body').addClass('lock-modal');
                }

                if (!$(options.target).hasClass('open-modal')) {
                    $(options.target).addClass('open-modal');
                }

            }, options.openDelay);
        }

        function _closeModal() {
            if ($('html, body').hasClass('lock-modal')) {
                $('html, body').removeClass('lock-modal');
            }

            if ($(options.target).hasClass('open-modal')) {
                $(options.target).removeClass('open-modal');
                $(options.target).children('.modal-content').html('');
            }
        }
        // set modal width
        $(options.target).find('.modal-content').css({"width": options.width});

        $(options.target).find('.close').click(function (e) {
            e.preventDefault();
            _closeModal();
        });

        this.click(function (e) {
            e.preventDefault();
            _openModal();
        });

        $(window).click(function (e) {
            if ($(e.target).hasClass('modal')) {
                e.preventDefault();
                _closeModal();
            }
        });
        if (options.isOpen) {
            _openModal();
        }

    };


    // test my plugin
    $("#openSmallModal").modal({
        target: '#smallModal',
        isOpen: false
    });

    $("#openLargeModal").modal({
        width: '80%',
        target: '#largeModal',
        isOpen: false,
        openDelay: 0
    });

})(jQuery);
