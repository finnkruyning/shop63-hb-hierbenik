(function ($) {
        

    $(document).ready(function(){
        
        $('.js-open-in-modal').modal({
            width: '80%',
            target: '#video-modal',
            isOpen: false,
            openDelay: 0
        });

        $(window).scroll(function(){

            // ON SCROLL PLACE MENU
            stickIt();

        });

        // CLICK SCROLL TO LINK, USE DATA ATRIBUTE AS ID
        $('#video-page').on('click', '.js-scroll-to', function(){
            
            var that = this;

            goToByScroll($($(that).data('scroll-to-link')), 60);
            
            // MAINTAIN BOTH ORIGINAL AND CLONED MENUS
            $('[data-scroll-to-link="'+$(that).data('scroll-to-link')+'"]').siblings().removeClass('active');
            $('[data-scroll-to-link="'+$(that).data('scroll-to-link')+'"]').addClass('active');

            // REMOVE ACTIVE STATE AFTER TIMEOUT
            setTimeout(function(){
                $('[data-scroll-to-link="'+$(that).data('scroll-to-link')+'"]').removeClass('active');
            }, 2500);

        });
  

        // STICK PREMIUM MENU TO TOP ON SCROLL
        // Create a clone of the menu, right next to original.
        $('.video-page-header__menu').addClass('original').clone().insertAfter('.video-page-header__menu').addClass('cloned').removeClass('original');
        stickIt();

        // LOAD IMAGES FOR ALL YOUTUBE 
        $('.js-video').each(function(){

            var that = this;
            var youTubeCode = checkYtUrl($(that).children('a').attr('href'));
            var imageSource = "https://img.youtube.com/vi/"+ youTubeCode +"/sddefault.jpg";
            var image = new Image();
            
            image.src = imageSource;
            image.addEventListener( "load", function() {
                $(image).css({'opacity': '0'});
                $(that).prepend( image );
                $(that).children('a').hide();
                $(image).animate({'opacity': '1' }, 300)
            });
            $(that).on( "click", function() {

                var iframe = document.createElement( "iframe" );

                iframe.setAttribute( "frameborder", "0" );
                iframe.setAttribute( "allowfullscreen", "" );
                iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ youTubeCode +"?rel=0&showinfo=0&autoplay=1" );

                if ($(that).hasClass('js-open-in-modal')) {
                    $('#video-modal .modal-content').html('');
                    $('#video-modal .modal-content').append( iframe );
                } else {
                    $(that).html('');
                    $(that).append( iframe );
                }

            });

 
        });

        function goToByScroll(elm, offset) {
            $('html,body').animate({
                scrollTop: (elm.offset().top - offset)+"px"},
            'slow');
        }

        // CHECK WHICH MENU TO SHOW, FIXED OR INLINE
        function stickIt() {
            if($('.original').length > 0){
            
                var orgElementPos = $('.original').offset();
                var orgElementTop = orgElementPos.top;
                var headerOffset = 1;               

                if ($(window).scrollTop() + headerOffset >= (orgElementTop)) {
                    
                    // scrolled past the original position; now only show the cloned, sticky element.

                    // Cloned element should always have same left position and width as original element.     
                    orgElement = $('.original');
                    coordsOrgElement = orgElement.offset();
                    leftOrgElement = coordsOrgElement.left;  
                    widthOrgElement = orgElement.css('width');
                    $('.cloned').css('left',leftOrgElement+'px').css('width',widthOrgElement).addClass('show');

                } else {

                    // not scrolled past the menu; only show the original menu.
                    $('.cloned').removeClass('show');
                }

            }
        }

        function checkYtUrl(url){
            var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&amp;amp;amp;v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false ;
        }

    });
})(jQuery);

