$(document).ready(function(){
    $('body').append('<div class="getswatches" style="position: fixed; top: 15px; right: 15px; z-index: 100000;">Kleuren laden</div>');
    
    $('.getswatches').on('click', function(){
        get_swatches();
    });
});

function get_swatches(){
    $('.thumbnail-card-img img').each(function(){
        if(!$(this).parent().find('span').length) {
            var img = new Image();
            img.setAttribute('src', $(this).attr('src'));
            var vibrant = new Vibrant(img, 64, 2);
            var swatches = vibrant.swatches()
            $(this).after('<div class="results" />');
            for (var swatch in swatches)
                if (swatches.hasOwnProperty(swatch) && swatches[swatch])
                    $(this).next('.results').append('<span title="'+swatches[swatch].getHex()+'" style="display: inline-block; color: #fff; text-align: center; width: 30px; height: 30px; background:'+swatches[swatch].getHex()+'; margin-right: 5px;"><span class="amount" style="color:'+swatches[swatch].getTitleTextColor()+'">'+swatches[swatch].getPopulation()+'</span></span>');
        
        }
    });
    
    
}