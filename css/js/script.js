jQuery(function(){

    // remove id
    if(jQuery('#pagecontent > #pagebody').length){
        jQuery('#pagecontent > #pagebody').removeAttr('id');
    }

    if(jQuery('#categories > #sidebar').length){
        jQuery('#categories > #sidebar').removeAttr('id');
    }

	if(jQuery("input[type='submit']").length > 0 ) {jQuery("input[type='submit']").wrap("<span class='btnsubmit'>");}

	var basket_text = '';var basket_count = '';var logout = '';var account = '';var help = '';var login = '';var hblock = '';var hsearch = '';
	var href = jQuery("#headerbasket").attr('href');
	basket_text += '<li>' + jQuery("#basket_text").html() + '</li>';
	basket_count += '<li><a href="'+href+'"><i class="ht ht-basket"></i><span>' + jQuery("#basket_count").html() + '</span></a></li>';
	if (jQuery("#headerlogout").length) logout += '<li><a href="/logout"><i class="ht ht-logout"></i><span>' + jQuery("#headerlogout span").html() + '</span></a></li>';
	if (jQuery("#headeraccount").length) account += '<li><a href="/account"><i class="ht ht-account"></i><span>' + jQuery("#headeraccount span").html() + '</span></a></li>';
	help += '<li class="help"><a><i class="ht ht-help1"></i><span>' + jQuery("#headerhelp span").html() + '</span></a></li>';
	contact = '<li><a href="/contact"><i class="ht ht-contact"></i><span>Contact</span></a></li>';
	if (jQuery("#headerlogin").length) login += '<li><a href="/login" onclick="open_float_login(); return false;"><i class="ht ht-login inloggen"></i><span>' + jQuery("#headerlogin span").html() + '</span></a></li>';
	var hnav = '<ul id="hnav">' + contact + help + login + account + logout + basket_count + '</ul>';

	if(jQuery(".hsearch").length){hsearch = jQuery(".hsearch").html();}

	if(jQuery('#header .hblock').length) {hblock += '<div class="hblock clearfix">' + jQuery("#header .hblock").html() + '</div>';}
	jQuery("#wrapper").before('<div id="topbar"><div class="container clearfix"><a href="#mainnav" class="phone-menu"><span>Menu</span></a>' + hblock + hsearch + hnav + '</div></div>');

	// Move crumbs block
	if (jQuery("#crumbs").length){var crumbs = jQuery('#crumbs').html();jQuery("#categories").before('<div class="breadcrumbs xs-hide xsm-hide">' + crumbs + '</div>');jQuery("#crumbs").remove();}
	if (jQuery("#body_contact").length){jQuery("#categories").before('<div class="breadcrumbs"><a href="/">Home</a>  Contact</div>');}

	// Move visual block
	if (jQuery(".visual").length > 0) { var $visual = jQuery(".visual"); var $h = '<div id="visual">' + $visual.html() + '</div>'; jQuery("#header").after($h);jQuery(".visual").remove();}else{jQuery("#header").addClass("border");}
	if (jQuery(".visual-page").length > 0) { var $visual = jQuery(".visual-page"); var $h = '<div id="visual-page">' + $visual.html() + '</div>'; jQuery("#header").after($h);jQuery(".visual-page").remove();}else{jQuery("#header").addClass("border");}

	if (jQuery('#body_home #pagecontent form').length > 0){jQuery('#body_home #pagecontent form').addClass('container');}

	//Hulp item aanpassen
	$('li.help span').text('Info & Prijzen');

	//Foldout menu info
	$("li.help a").changeElementType("span");
	$('li.help').append('<span class="ardown"></span><div class="borderbreak"></div><ul class="foldout"><li><a href="/prijzen">Prijzen</a></li><li><a href="/proefdruk">Proefdruk</a></li><li><a href="/enveloppen-info">Enveloppen</a></li><li><a href="/betalen">Betalen</a></li><li><a href="/bezorging">Bezorging</a></li><li><a href="/garantie">Garantie</a></li><li><a href="/privacy">Privacy</a></li><li><a href="/faq">FAQ</a></li><li><a href="/algemene_voorwaarden">Algemene voorwaarden</a></li></ul>');
	$('li.help').hover(function(){
		$(this).addClass('active');
		$(this).find('ul').show();
		$(this).find('.borderbreak').show();
	}, function(){
		$(this).removeClass('active');
		$(this).find('ul').hide();
		$(this).find('.borderbreak').hide();
	});

	if(jQuery('#body_login').length >0){
		jQuery('#mid #categories').before('<div class="breadcrumbs"><a href="/">home</a> <span>&gt;</span> Login</div>');
	}

	// Search in header
	jQuery(".ico-search").click(function(e){ e.preventDefault();if (jQuery(this).hasClass("opened")) {jQuery(".form").animate({'opacity':0,'padding-right':0,'width':0},1500);jQuery(this).removeClass("opened");} else {jQuery(".form").animate({'opacity':1,'padding-right':'30px','width':'169px'},1500);jQuery(this).addClass("opened");}});

	// jQuery( window ).load(function() {
        function initMasonry() {
            var masonryOptions = {
              itemSelector: '.item',
            };

            var $grid = $('.blog-list').masonry( masonryOptions )

            if ($(window).width() > 640) {
                $grid = $('.blog-list').masonry( masonryOptions );
            } else {
                $grid.masonry('destroy');
            }
        }

        initMasonry();
        var timeOutMasonry;
        $(window).on('resize.initMasonry', function() {
            clearTimeout(timeOutMasonry);
            timeOutMasonry = setTimeout(function() {
                initMasonry();
            }, 300);
        })

	// });
	if (jQuery(window).width() < 1171) {
		jQuery("#mainnav").mmenu({
			extensions: ["pageshadow", "theme-white"],
			sectionIndexer: {
				add: true,
				addTo: "[id*='contacts-']"
			},
			dividers: {
				add: true,
				addTo: "[id*='contacts-']",
				fixed: true
			},
			navbar: false
		}).on('click', 'a[href^="#/"]', function () {
				alert("Thank you for clicking, but that's a demo link.");
				return false;
			}
		);
	}
	// Dropdown for mainmenu
	if(jQuery("#mainnav").length){ jQuery("#mainnav ul li").find("ul").parent().addClass("has-child");}
	$('#mainnav li.has-child').hover(function() {$(this).find('ul').fadeIn();$(this).addClass('active');}, function() {$(this).find('ul').fadeOut();$(this).removeClass('active');});

	jQuery("#body_account #pagecontent p .button:eq(2)").hide();
	jQuery("#body_account #pagecontent p .button:eq(3)").hide();
	jQuery("#body_account #pagecontent p .button:eq(4)").css("margin-top","-68px");
	jQuery("#body_account #pagecontent p br:eq(5)").hide();
	jQuery("#body_account #pagecontent p br:eq(2)").hide();
	jQuery("#body_account #pagecontent p br:eq(3)").hide();
	jQuery("#body_account #pagecontent p br:eq(4)").hide();


    if (jQuery('#card_previews').length && jQuery('#categories').length && !jQuery('#body_home').length ) {

        if ( jQuery('#sideblock').length ) {
            jQuery('#categories').append(jQuery('#sideblock'));
        }

        // jQuery('#categories').append('<div id="sideblock"></div>');
        // jQuery('#sideblock').append(jQuery("#pagecontent h1"));
        // jQuery('#sideblock').append(jQuery("#pagecontent .shopAlert").next('div'));

    }



	// Dropdown for lnav
	//if(jQuery("body").hasClass("show-tagged")){jQuery("#leftnav p.category").addClass("drop");
    jQuery(".block-icon").hide();
	jQuery(".drop").click(function(e){
		e.preventDefault();
		var $t = jQuery(this);
		if($t.hasClass("opened")){
			jQuery(".block-icon").slideUp();
			$t.removeClass("opened");
		} else {
			jQuery(".block-icon").slideDown();
			$t.addClass("opened");
		}
	});
	if(jQuery("#filter_menu").length>0){jQuery(".tag-group-ul li h3").click(function(){var i=jQuery(this);if(i.hasClass("closed")){i.next("ul").slideDown();i.removeClass("closed")}else{i.next("ul").slideUp();i.addClass("closed")}})}
	if (jQuery("#lnav").length){jQuery("#lnav li").find("ul").parent().addClass("has-child");}
	jQuery("#lnav .has-child h3 a").click(function(e){
		e.preventDefault();
		var $this = jQuery(this);
		if ($this.parent().hasClass("selected")) {
			$this.parent().next('ul').slideUp();
			$this.parent().removeClass("selected");
		} else {
			jQuery("#lnav ul").slideUp();
			jQuery("#lnav h3").removeClass('selected');
			$this.parent().next('ul').slideDown();
			$this.parent().addClass("selected");
		}
	});

	var pathname = window.location.pathname;
	pathname_a = pathname.split("/");
	jQuery("#lnav .has-child ul li").each(function(){
		var h = jQuery(this).find('a').attr('href');
		if (h == pathname){jQuery(this).addClass('active');jQuery(this).parent().prev().addClass('selected');jQuery(this).parent().css('display','block');}
	});
	//Refer afhankelijke functies
    if (document.referrer.indexOf('hierbenik') > -1) {
        $('#body_design .xs-12.xsm-12.sm-7.md-7.lg-7').prepend('<a href="#" class="xs-hide xsm-hide" onclick="window.history.go(-1); return false;">« Terug naar overzicht</a>');
        $('div.accordion.first a').addClass('active');
        $('div.accordion.first div.accordion-body').addClass('show');
    } else {
        var gBackLink = window.location.href;
        gBackLink = gBackLink.substr(0, gBackLink.lastIndexOf("/"));
        $('#body_design .xs-12.xsm-12.sm-7.md-7.lg-7').prepend('<a class="xs-hide xsm-hide" href=' + gBackLink + '>« Terug naar overzicht</a>');
        $('div.accordion:last a').addClass('active');
        $('div.accordion:last div.accordion-body').addClass('show');
    }

    //login pagina
    if($('#body_login').length) {
        $('td.field input').each(function(){
            var valVal = $(this).attr('id');
            var newVal = "";
            switch (valVal)
            {
               case 'email_login':
               newVal = "E-mailadres";
               break;

               case 'password':
               newVal = "Wachtwoord";
               break;

               case 'email_reset_password':
               newVal = "E-mailadres";
               break;

               case 'name':
               newVal = "Naam";
               break;

               case 'email_register':
               newVal = "E-mailadres";
               break;

               case 'password1':
               newVal = "Wachtwoord";
               break;

               case 'password2':
               newVal = "Herhaal wachtwoord";
               break;
            }
            if(newVal != "") {
                $(this).attr("placeholder", newVal);
            }
        });
    }

    //Design wijzigingen
    if($('#body_design').length) {
        $('#card_previews_horizontal img').each(function(){
            var rsrc = $(this).attr('real-src');
            $(this).attr('src', rsrc);
        });

        $('#card_previews_horizontal').parent().addClass('cardrail');
    }
    $('#voor_preview').clone().prop('id', 'voor_preview_envelop').prependTo('#prevWrap');
    $('#voor_preview').hide();
    $('#voor_preview_envelop').append('<span class="envgroot" />');

    $('#voor, #binnen').unbind();
    $('#voor').clone().prop('id', 'voorenv').prependTo('.select_preview');
    $('#voorenv').wrap('<span class="envview" />');
    $('.envview').append('<span class="env" />');

      //Design gallery functie
    if($('div.select_preview').length){
        $('div.select_preview>span').on('click', function(){
            var thisdex = $(this).index();
            $('#prevWrap>div').hide();
            $('#prevWrap>div:eq('+thisdex+')').show();
        })
    }

    //Tijdelijke fix preview click
    var prevClick = $('#choose_preview').attr('href');
    $('#prevWrap').on('click', function(){
        window.location.href = prevClick;
    });

	$('.zelf-list li').click(function(){
		window.location.href = $(this).find('a').attr('href');
	});

     //Refer afhankelijke functies
    if (document.referrer.indexOf('hierbenik') > -1) {
        $('div.detAccord .innerHtml').hide();
        $('div.detAccord.first .innerHtml').show();
        $('div.detAccord.first .detAccordTitle').addClass('active');
    } else {
        $('div.detAccord .innerHtml').hide();
        $('div.detAccord:eq(2) .innerHtml').show();
        $('div.detAccord:eq(2) .detAccordTitle').addClass('active');
    }


        // check type of page should be added to first dive
    // has id fluid-template
    // has id sidebar-template
    if ($('#template-fluid').length) {
        $('#pagecontent').removeClass("sm-9 md-9").addClass("sm-12 md-12");
        $('#categories').remove();
    } else if ($('#pagecontent').find('#template-sidebar').length) {
        $('#pagecontent').removeClass("sm-12 md-9").addClass("sm-9 md-9");
    } else {
        $('#pagecontent').removeClass("sm-12 md-12").addClass("sm-9 md-9");
    }

    // hover text kleur
    if($('.kleur')) {
        $('.kleur .tag-li').each(function () {
            var $hoverText= $(this).find('label').clone().children().remove().end().text().trim();
            $(this).append('<span class="tooltip"></span>');
            $(this).find('.tooltip').html($hoverText);
            console.log($hoverText)
        })
    }
});

$(window).load(function(){
    //Filter scripts
    if(window.location.hash) {
        var filHash = window.location.hash.substring(1);
        filHash = filHash.split("-");

        $.each(filHash, function(i,val) {
            $('.tag-li label:contains('+val+')').parent().find('input').prop('checked', true);
        })
        clickFilter()
    }
});


$.fn.changeElementType = function(newType) {
	var attrs = {};

	$.each(this[0].attributes, function(idx, attr) {
		attrs[attr.nodeName] = attr.nodeValue;
	});

	this.replaceWith(function() {
		return $("<" + newType + "/>", attrs).append($(this).contents());
	});
}

$('#body_account .cols-multi > div:nth-child(3)').remove();
